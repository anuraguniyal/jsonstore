from google.appengine.ext import db

class Document(db.Expando):
    created_at = db.DateTimeProperty(auto_now_add=True)
    edited_at = db.DateTimeProperty(auto_now=True)
class JSBaseException(Exception): pass

class APIError(JSBaseException):
    error_code = "ERR_1"
    
class UnkownFieldType(APIError): 
    error_code = "ERR_2"

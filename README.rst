JsonStore
==========

JsonStore is a JSON document store and provide API for creating, updating and deleting documents. It uses Google App Engine as backend.

Usage
------

Simply copy or link jsonstore folder to your app, and in your WSGIApplication add routes by calling `jsonstore.get_routes` e.g.

::

     import jsonstore
     
     routes = [
          ...# your routes
     ]
     
     # get routes from jsonstore and add them to your route list
     routes.extend(jsonstore.get_routes('/jstore')) #jstore is the prefix for URLs
     
     app = myapp.MyApplication(routes, debug=True)


jsonstore adds following routes with names, which you can use in `webapp2.uri_for`

+------------+----------------------+------------------+ 
| API        | URL                  | Name             | 
+============+======================+==================+ 
| Create Doc | {prefix}/createdoc   | api_create_doc   | 
+------------+----------------------+------------------+ 
| Update Doc | {prefix}/updatedoc   | api_update_doc   | 
+------------+----------------------+------------------+ 


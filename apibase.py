import logging
import traceback

import json
import webapp2

from apiexceptions import JSBaseException, APIError

class json_api(object):
    """
    our class which will be used as decorator for json apis 
    """
    def __init__(self):
        pass
        
    def __call__(self, apifunc):
        """
        decorator for json api
        """

        def _apifunc(self, *args, **kwargs):

            data_out = {}
            data_out['success'] = False
            data_out['error'] = ''
            data_out['error_code'] = ''
            try:
                data_in = json.loads(self.request.body)
                response = apifunc(self, data_in, data_out, *args, **kwargs)
                # if api returned any reponse use it
                if response is not None:
                    return response
                data_out['success'] = True

            except APIError,e:
                data_out['error'] = unicode(e) 
                data_out['error_code'] = e.error_code               
            except JSBaseException,e:
                data_out['error'] = unicode(e)
            except Exception,e:
                data_out['error'] = unicode(e)
                logging.error(traceback.format_exc())

            try:
                data = json.dumps(data_out)
            except Exception,e:
                err = "error in json serialization: %s"%e
                logging.error(err)
                logging.debug(traceback.format_exc())
                data = json.dumps({'success':False, 'error':err})

            response =  webapp2.Response(content_type='application/json')
            response.body = data
            return response
            
        return _apifunc
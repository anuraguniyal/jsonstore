import logging
import json

import webapp2

import apibase
from document import FieldDocument, DocumentQuery

class CreateDocAPI(webapp2.RequestHandler):

    @apibase.json_api()
    def post(self, data_in, data_out):
        document = FieldDocument()
        document.set_fields(data_in)
        data_out['doc'] = document.get_as_dict()
        logging.info(self.request.body)

class UpdateDocAPI(webapp2.RequestHandler):

    @apibase.json_api()
    def post(self, data_in, data_out):
        document = FieldDocument()
        document.set_fields(data_in)
        data_out['doc'] = document.get_as_dict()
            
            
class QueryDocAPI(webapp2.RequestHandler):

    @apibase.json_api()
    def post(self, data_in, data_out):
        query = DocumentQuery(data_in)
        data_out['data'] = query.get_results_dict()
    
    
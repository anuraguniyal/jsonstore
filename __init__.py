import webapp2
import apiviews
def get_routes(prefix):
    return [
        webapp2.Route('%s/createdoc'%prefix, apiviews.CreateDocAPI, name='api_create_doc'),
        webapp2.Route('%s/updatedoc'%prefix, apiviews.UpdateDocAPI, name='api_update_doc'),
        webapp2.Route('%s/querydoc'%prefix, apiviews.QueryDocAPI, name='api_query_doc')
    ]


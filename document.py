import decimal
import datetime
from collections import defaultdict

from google.appengine.ext import db

from models import Document
from apiexceptions import UnkownFieldType

class FieldDocument(object):
    """
    a helper class to set fields for a document
    """
    
    def convert_string(value):
        return unicode(value)
    
    def convert_text(value):
        return db.Text(value)
    
    DEFAULT_TYPE = 'string'
    TYPES = {
        'string':convert_string,
        'Text':convert_text,
    }    
    
    def __init__(self, doc=None):
        self.doc = doc if doc is not None else Document()
        
    def set_fields(self, data):
        for field, value_type in data.iteritems():
            if 'type' in value_type:
                _type = value_type['type']
            else:
                _type = self.DEFAULT_TYPE
                
            value = value_type['value']
            dbvalue = self.TYPES[_type](value)
            setattr(self.doc, field, dbvalue)
            
        self.doc.put()
        
    def get_as_dict(self):
        data = db.to_dict(self.doc)
        # convert data so that can be json encoded, we could do it in JSONEncode but here
        # in future we may do different conversion based on db type or user preferences
        typed_data = {}
        for field in data:
            value = data[field]
            if isinstance(value, basestring):
                _type = 'string'
            else:
                _type = value.__class__.__name__

            if isinstance(value, (decimal.Decimal)):
                value = str(value)
            elif isinstance(value, (datetime.datetime, datetime.date)):
                value = value.isoformat()
            elif isinstance(value, datetime.time):
                value = value.strftime('%H:%M:%S') 
     
            typed_data[field] = {'value':value, 'type':_type}
            
        return typed_data
    
class DocumentQuery(object):
    """
    creates a db query out of user params
    """
    def __init__(self, params):
        self.query = db.Query(Document)
        
        self.fetch_count = 100
        for name, data in params.iteritems():
            if name == 'filter':
                self.query.filter(data['field'], data['value'])
            elif name == 'order':
                self.query.order(data)
            elif name == 'count':
                self.fetch_count = data
                
    def get_results_dict(self):
        data = {}
        data['results'] = results = []
        for doc in self.query.fetch(self.fetch_count):
            results.append(FieldDocument(doc).get_as_dict())
        data['count'] = len(results)
        data['cursor'] = self.query.cursor()
        
        return data
    